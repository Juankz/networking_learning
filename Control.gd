extends Control

onready var label = $Label

func host_server(port):
	var host = NetworkedMultiplayerENet.new()
	host.create_server(port)
	get_tree().set_network_peer(host)

func join_game(ip, port):
	var host = NetworkedMultiplayerENet.new()
	host.create_client(ip, port)
	get_tree().set_network_peer(host)
	
func end_game():
	get_tree().set_network_peer(null)

func _on_Server_pressed():
	host_server(1300)
	$VBoxContainer/Server.disabled = true

func _on_Client_pressed():
	join_game("127.0.0.1",1300)
	$VBoxContainer/Client.disabled = true	

func update_text(txt):
	label.text = label.text + '\n' + txt
	
sync func sync_update_text(txt):
	update_text('sync '+txt)
	
remote func remote_update_text(txt):
	update_text('remote '+txt)
	
master func master_update_text(txt):
	update_text('master '+txt)
	
slave func slave_update_text(txt):
	update_text('slave '+txt)

master func kill_player(txt):
	update_text('kill player '+txt)


func _on_Send_ping_pressed():
#	rpc('sync_update_text','ping!')
#	rpc('remote_update_text','ping!')
	rpc('master_update_text','ping!')
#	rpc('slave_update_text','ping!')

func _on_SendSlavePing_pressed():
	rpc('slave_update_text','ping!')


func _on_SendRemotePing_pressed():
	rpc('remote_update_text','ping!')
